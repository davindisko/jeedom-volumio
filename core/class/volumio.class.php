<?php

/* This file is part of Jeedom.
 *
 * Jeedom is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jeedom is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Jeedom. If not, see <http://www.gnu.org/licenses/>.
 */

/* * ***************************Includes********************************* */
require_once dirname(__FILE__) . '/../../../../core/php/core.inc.php';

class volumio extends eqLogic {
    /*     * *************************Attributs****************************** */

    public static $_widgetPossibility = array('custom' => true, 'custom::layout' => false);

    /*     * ***********************Methode static*************************** */

    public static function cron() {
        foreach (eqLogic::byType('volumio') as $eqLogic) {
            $autorefresh = $eqLogic->getConfiguration('autorefresh');
            if ($eqLogic->getIsEnable() == 1 && $autorefresh != '') {
                try {
                    $c = new Cron\CronExpression($autorefresh, new Cron\FieldFactory());
                    if ($c->isDue()) {
                        try {
                            $eqLogic->refresh();
                        } catch (\Exception $exc) {
                            log::add('volumio', 'error', __('Erreur pour ', __FILE__) . $eqLogic->getHumanName() . ' : ' . $exc->getMessage());
                        }
                    }
                } catch (\Exception $exc) {
                    log::add('volumio', 'error', __('Expression cron non valide pour ', __FILE__) . $eqLogic->getHumanName() . ' : ' . $autorefresh);
                }
            }
        }
    }

    /*
     * Fonction exécutée automatiquement toutes les heures par Jeedom
      public static function cronHourly() {

      }
     */

    /*
     * Fonction exécutée automatiquement tous les jours par Jeedom
      public static function cronDayly() {

      }
     */



    /*     * *********************Méthodes d'instance************************* */

    public function preInsert() {
        $this->setCategory('multimedia', 1);
    }

    /*public function postInsert() {
        
    }

    public function preSave() {
        
    }*/

    public function postSave() {
        if (!$this->getId()) {
            return;
        }

        $refresh = $this->getCmd(null, 'refresh');
        if (!is_object($refresh)) {
            $refresh = new scriptCmd();
            $refresh->setLogicalId('refresh');
            $refresh->setIsVisible(1);
            $refresh->setName(__('Rafraichir', __FILE__));
        }
        $refresh->setType('action');
        $refresh->setSubType('other');
        $refresh->setEqLogic_id($this->getId());
        $refresh->save();

        // PREV
        $previous = $this->getCmd(null, 'prev');
        if (!is_object($previous)) {
            $previous = new volumioCmd();
            $previous->setLogicalId('prev');
            $previous->setIsVisible(1);
            $previous->setName(__('Précédent', __FILE__));
        }
        $previous->setType('action');
        $previous->setSubType('other');
        $previous->setConfiguration('request', 'prev');
        $previous->setEqLogic_id($this->getId());
        $previous->save();

        // NEXT
        $next = $this->getCmd(null, 'next');
        if (!is_object($next)) {
            $next = new volumioCmd();
            $next->setLogicalId('next');
            $next->setIsVisible(1);
            $next->setName(__('Suivant', __FILE__));
        }
        $next->setType('action');
        $next->setSubType('other');
        $next->setConfiguration('request', 'next');
        $next->setEqLogic_id($this->getId());
        $next->save();

        // PLAY
        $play = $this->getCmd(null, 'play');
        if (!is_object($play)) {
            $play = new volumioCmd();
            $play->setLogicalId('play');
            $play->setIsVisible(1);
            $play->setName(__('Lecture', __FILE__));
        }
        $play->setType('action');
        $play->setSubType('other');
        $play->setConfiguration('request', 'play');
        $play->setEqLogic_id($this->getId());
        $play->save();

        // TOGGLE
        $toggle = $this->getCmd(null, 'toggle');
        if (!is_object($toggle)) {
            $toggle = new volumioCmd();
            $toggle->setLogicalId('toggle');
            $toggle->setIsVisible(1);
            $toggle->setName(__('Play/Pause', __FILE__));
        }
        $toggle->setType('action');
        $toggle->setSubType('other');
        $toggle->setConfiguration('request', 'toggle');
        $toggle->setEqLogic_id($this->getId());
        $toggle->save();

        // PAUSE
        $pause = $this->getCmd(null, 'pause');
        if (!is_object($pause)) {
            $pause = new volumioCmd();
            $pause->setLogicalId('pause');
            $pause->setIsVisible(1);
            $pause->setName(__('Pause', __FILE__));
        }
        $pause->setType('action');
        $pause->setSubType('other');
        $pause->setConfiguration('request', 'pause');
        $pause->setEqLogic_id($this->getId());
        $pause->save();

        // STOP
        $stop = $this->getCmd(null, 'stop');
        if (!is_object($stop)) {
            $stop = new volumioCmd();
            $stop->setLogicalId('stop');
            $stop->setIsVisible(1);
            $stop->setName(__('Stop', __FILE__));
        }
        $stop->setType('action');
        $stop->setSubType('other');
        $stop->setConfiguration('request', 'stop');
        $stop->setEqLogic_id($this->getId());
        $stop->save();

        // VOLUME SET
        $setVolume = $this->getCmd(null, 'setVolume');
        if (!is_object($setVolume)) {
            $setVolume = new volumioCmd();
            $setVolume->setLogicalId('setVolume');
            $setVolume->setIsVisible(1);
            $setVolume->setName(__('Set volume', __FILE__));
        }
        $setVolume->setType('action');
        $setVolume->setSubType('slider');
        $setVolume->setConfiguration('request', 'volume=#slider#');
        $setVolume->setEqLogic_id($this->getId());
        $setVolume->save();

        // INFO
        // ALL INFO
//        $info = $this->getCmd(null, 'info');
//        if (!is_object($info)) {
//            $info = new volumioCmd();
//            $info->setLogicalId('info');
//            $info->setIsVisible(1);
//            $info->setName(__('Info', __FILE__));
//        }
//        $info->setType('info');
//        $info->setEventOnly(1);
//        $info->setSubType('string');
//        $info->setEqLogic_id($this->getId());
//        $info->save();
        // STATUS
        $status = $this->getCmd(null, 'status');
        if (!is_object($status)) {
            $status = new volumioCmd();
            $status->setLogicalId('status');
            $status->setIsVisible(1);
            $status->setName(__('Status', __FILE__));
        }
        $status->setType('info');
        $status->setEventOnly(1);
        $status->setSubType('string');
        $status->setConfiguration('request', 'status');
        $status->setEqLogic_id($this->getId());
        $status->save();

        // SERVICE
        $service = $this->getCmd(null, 'service');
        if (!is_object($service)) {
            $service = new volumioCmd();
            $service->setLogicalId('service');
            $service->setIsVisible(1);
            $service->setName(__('Service', __FILE__));
        }
        $service->setType('info');
        $service->setEventOnly(1);
        $service->setSubType('string');
        $service->setConfiguration('request', 'service');
        $service->setEqLogic_id($this->getId());
        $service->save();

        // TITLE
        $title = $this->getCmd(null, 'title');
        if (!is_object($title)) {
            $title = new volumioCmd();
            $title->setLogicalId('title');
            $title->setIsVisible(1);
            $title->setName(__('Title', __FILE__));
        }
        $title->setType('info');
        $title->setEventOnly(1);
        $title->setSubType('string');
        $title->setConfiguration('request', 'title');
        $title->setEqLogic_id($this->getId());
        $title->save();

        // ARTIST
        $artist = $this->getCmd(null, 'artist');
        if (!is_object($artist)) {
            $artist = new volumioCmd();
            $artist->setLogicalId('artist');
            $artist->setIsVisible(1);
            $artist->setName(__('Artist', __FILE__));
        }
        $artist->setType('info');
        $artist->setEventOnly(1);
        $artist->setSubType('string');
        $artist->setConfiguration('request', 'artist');
        $artist->setEqLogic_id($this->getId());
        $artist->save();

        // ALBUM
        $album = $this->getCmd(null, 'album');
        if (!is_object($album)) {
            $album = new volumioCmd();
            $album->setLogicalId('album');
            $album->setIsVisible(1);
            $album->setName(__('Album', __FILE__));
        }
        $album->setType('info');
        $album->setEventOnly(1);
        $album->setSubType('string');
        $album->setConfiguration('request', 'album');
        $album->setEqLogic_id($this->getId());
        $album->save();

        // ALBUM ART
        $albumart = $this->getCmd(null, 'albumart');
        if (!is_object($albumart)) {
            $albumart = new volumioCmd();
            $albumart->setLogicalId('albumart');
            $albumart->setIsVisible(1);
            $albumart->setName(__('Album art', __FILE__));
        }
        $albumart->setType('info');
        $albumart->setEventOnly(1);
        $albumart->setSubType('string');
        $albumart->setConfiguration('request', 'albumart');
        $albumart->setEqLogic_id($this->getId());
        $albumart->save();

        // VOLUME
        $volume = $this->getCmd(null, 'volume');
        if (!is_object($volume)) {
            $volume = new volumioCmd();
            $volume->setLogicalId('volume');
            $volume->setIsVisible(0);
            $volume->setName(__('Volume', __FILE__));
        }
        $volume->setType('info');
        $volume->setEventOnly(1);
        $volume->setSubType('numeric');
        $volume->setConfiguration('request', 'volume');
        $volume->setEqLogic_id($this->getId());
        $volume->save();
    }

    public function refresh() {
        foreach ($this->getCmd('info') as $cmd) {
            $cmd->refresh();
        }
    }

    public function preUpdate() {
        if ($this->getConfiguration('ipAddr') == '') {
            throw new \Exception(__('L\'adresse ne peut etre vide', __FILE__));
        }
    }

    /*public function postUpdate() {
        
    }

    public function preRemove() {
        
    }

    public function postRemove() {
        
    }*/

    public function toHtml($_version = 'dashboard') {
        $replace = $this->preToHtml($_version);
        if (!is_array($replace)) {
            return $replace;
        }
        $version = jeedom::versionAlias($_version);
        if ($this->getDisplay('hideOn' . $version) == 1) {
            return '';
        }

        $refresh = $this->getCmd(null, 'refresh');
        $replace['#refresh_id#'] = is_object($refresh) ? $refresh->getId() : '';

        $cmd_etat = $this->getCmd(null, 'status');

        if (is_object($cmd_etat)) {
            if ($cmd_etat->execCmd() == 'play') {
                $state_nb = 1;
            } else {
                $state_nb = 0;
            }
            $replace['#state_nb#'] = $state_nb;
            $replace['#status#'] = $cmd_etat->execCmd();
        }

        $cmd_titre = $this->getCmd(null, 'title');
        if (is_object($cmd_titre)) {
            if (is_object($cmd_titre)) {
                if (strlen($cmd_titre->execCmd()) > 17) {
                    $name = $cmd_titre->execCmd();
                } else {
                    $name = $cmd_titre->execCmd();
                }
                $replace['#orititre#'] = $cmd_titre->execCmd();
                $replace['#titre#'] = $name;
            }
        }

        $cmd_album = $this->getCmd(null, 'album');
        $cmd_artist = $this->getCmd(null, 'artist');
        if (is_object($cmd_album) && is_object($cmd_artist)) {
            if ($cmd_album->execCmd(null, 2) != '') {
                if (strlen($cmd_artist->execCmd(null, 2) . ' - ' . $cmd_album->execCmd(null, 2)) > 17) {
                    $albumartist = $cmd_artist->execCmd(null, 2) . ' - ' . $cmd_album->execCmd(null, 2);
                } else {
                    if ($cmd_artist->execCmd(null, 2) == 'Plugin') {
                        $albumartist = $cmd_artist->execCmd(null, 2);
                    } else {
                        $albumartist = $cmd_artist->execCmd(null, 2) . ' - ' . $cmd_album->execCmd(null, 2);
                    }
                }
            } else {
                $albumartist = $cmd_artist->execCmd(null, 2);
            }

            $replace['#artistalbum#'] = $albumartist;
            if ($cmd_artist->execCmd(null, 2) == 'Plugin') {
                $replace['#oriartistalbum#'] = $cmd_artist->execCmd(null, 2);
            } else {
                $replace['#oriartistalbum#'] = $cmd_artist->execCmd(null, 2) . ' - ' . $cmd_album->execCmd(null, 2);
            }

            $cmd_img = $this->getCmd(null, 'albumart');
            $img = "http://" . $this->getConfiguration('ipAddr') . $cmd_img->execCmd(null, 2);

            if ($cmd_etat->execCmd() == 'stop') {
                $imgid = 'greysb';
            } else {
                $imgid = 'normalsb';
            }
            $replace['#albumart#'] = '<img id="' . $imgid . '" height="150" width="150" src="' . $img . '" />';
        }


        foreach ($this->getCmd('action') as $cmd) {
            $replace['#cmd_' . $cmd->getLogicalId() . '_id#'] = $cmd->getId();
        }

        return $this->postToHtml($_version, template_replace($replace, getTemplate('core', $version, 'eqLogic', 'volumio')));
    }

    /*
     * Non obligatoire mais ca permet de déclencher une action après modification de variable de configuration
      public static function postConfig_<Variable>() {
      }
     */

    /*
     * Non obligatoire mais ca permet de déclencher une action avant modification de variable de configuration
      public static function preConfig_<Variable>() {
      }
     */

    /*     * **********************Getteur Setteur*************************** */
}

class volumioCmd extends cmd {
    /*     * *************************Attributs****************************** */

    public static $_widgetPossibility = array('custom' => false);

    /*     * ***********************Methode static*************************** */


    /*     * *********************Methode d'instance************************* */

    public function dontRemoveCmd() {
        if ($this->getLogicalId() == 'refresh') {
            return true;
        }
        return false;
    }

    public function refresh() {
        if ($this->getType() != 'info') {
            return;
        }
        if (trim($this->getConfiguration('request')) == '') {
            return;
        }
        $this->getEqLogic()->checkAndUpdateCmd($this, $this->execute());
    }

    public function preSave() {
        if ($this->getLogicalId() == 'refresh') {
            return;
        }
        if ($this->getConfiguration('request') == '') {
            throw new \Exception(__('Le champ requête ne peut pas être vide', __FILE__));
        }
    }

    public function execute($_options = null) {
        if ($this->getLogicalId() == 'refresh') {
            $this->getEqLogic()->refresh();
            return;
        }
        $result = false;

        $request = $this->getConfiguration('request');

        if (trim($request) == '') {
            throw new \Exception(__('La requête ne peut pas être vide : ', __FILE__) . print_r($this, true));
        }

        $volumio = $this->getEqLogic();
        $ipAddr = $volumio->getConfiguration('ipAddr');
        $url = 'http://' . $ipAddr;

        if ($_options != null) {
            switch ($this->getType()) {
                case 'action':
                    switch ($this->getSubType()) {
                        case 'slider':
                            $request = str_replace('#slider#', $_options['slider'], $request);
                            break;
                        case 'color':
                            $request = str_replace('#color#', $_options['color'], $request);
                            break;
                        case 'message':
                            $replace = array('#title#', '#message#');
                            if ($this->getConfiguration('requestType') == 'http') {
                                $replaceBy = array(urlencode($_options['title']), urlencode($_options['message']));
                            } elseif ($this->getConfiguration('requestType') == 'script') {
                                $replaceBy = array($_options['title'], $_options['message']);
                            } else {
                                $replaceBy = array(escapeshellcmd($_options['title']), escapeshellcmd($_options['message']));
                            }
                            if ($_options['message'] == '' && $_options['title'] == '') {
                                throw new Exception(__('Le message et le sujet ne peuvent pas être vide', __FILE__));
                            }
                            $request = str_replace($replace, $replaceBy, $request);
                            break;
                    }
                    break;
            }
        }
//        $request = scenarioExpression::setTags($request);
        $request = trim(str_replace('\'', '', $request));

        switch ($this->getType()) {

            // ACTION VIA HTTP
            case 'action':
                $url = $url . "/api/v1/commands/?cmd=" . $request;
                $url = str_replace('"', '%22', $url);
                $url = str_replace(' ', '%20', $url);
                $request_http = new com_http($url);
                $result = trim($request_http->exec(2, 4));
                break;

            // INFO VIA JSON
            case 'info':
                $request = str_replace('"', '', $request);
                $url = $url . "/api/v1/getstate";
                $request_http = new com_http($url);

                $json = trim($request_http->exec(2, 4));
                try {
                    $json = json_decode($json, true);
                } catch (\Exception $e) {
                    $request_http = new com_http($url);
                    $json = trim($request_http->exec(2, 4));
                    $json = json_decode($json, true);
                }

                $tags = explode('>', $request);
                foreach ($tags as $tag) {
                    $tag = trim($tag);
                    if (isset($json[$tag])) {
                        $json = $json[$tag];
                    } elseif (is_numeric(intval($tag)) && isset($json[intval($tag)])) {
                        $json = $json[intval($tag)];
                    } elseif (is_numeric(intval($tag)) && intval($tag) < 0 && isset($json[count($json) + intval($tag)])) {
                        $json = $json[count($json) + intval($tag)];
                    } else {
                        $json = '';
                        break;
                    }
                }
                if (is_array($json)) {
                    $result = json_encode($json);
                } else {
                    $result = $json;
                }
                return $result;
        }

        if ($this->getType() == 'action') {
            sleep(1);
            foreach ($this->getEqLogic()->getCmd('info') as $cmd) {
                $value = $cmd->execute();
                log::add('volumio', 'debug', 'cmd : ' . json_encode($value));
                if ($cmd->execCmd(null, 2) != $cmd->formatValue($value)) {
                    $cmd->event($value);
                }
            }
        }
        log::add('volumio', 'debug', 'Result : ' . $result . " Type : " . $this->getType());
        return $result;
    }

    /*     * **********************Getteur Setteur*************************** */
}
